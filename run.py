import os

from app.main import create_app, db
from app import blueprint
from flask_migrate import Migrate
from flask_cors import CORS
from app.main.model import (
    user,
    talent,
    countries_cities,
    technologies,
    job,
    company,
    responsable,
    knowledges,
    talent_knowledges,
    course,
    empleo,
    inscription_courses,
    larioja_depto,
    level_knowledge,
    motivation,
    talent_empleo,
    talent_motivation,
    genre,
    education_level,
    education,
    provinces,
)

app = create_app(os.getenv("FLASK_ENV", "dev"))
app.register_blueprint(blueprint, url_prefix="/llankay/v1")

migrate = Migrate(app, db)

CORS(app, resources={r"/llankay/*": {"origins": "*"}})
