"""empty message

Revision ID: 4fac51d97e4c
Revises: 85df694cbcdb, 6668000dda91
Create Date: 2022-06-21 12:31:36.055491

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4fac51d97e4c'
down_revision = ('85df694cbcdb', '6668000dda91')
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
