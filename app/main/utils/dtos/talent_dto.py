from flask_restx import Namespace, fields


class TalentDto:
    api = Namespace("Talents", description="Talents related operations")
    talent = api.model(
        "talent",
        {
            "name": fields.String(required=True, description="talent's name"),
            "surname": fields.String(required=True, description="talent's surname"),
            "email": fields.String(required=True, description="talent's email address"),
            "dni": fields.String(required=True, description="talent's DNI"),
            "cellphone": fields.String(required=True, description="talent's cellphone"),
            "linkedin": fields.String(required=False, description="talent's linkedin"),
            "other_webpage": fields.String(required=False, description="talent's webpage"),
            "bio": fields.String(required=False, description="talent's bio"),
            "github": fields.String(required=False, description="talent's Github"),
            "gitlab": fields.String(required=False, description="talent's Gitlab"),
            "larioja_depto_id": fields.Integer(
                required=False, description="talent's residence in La Rioja"
            ),
            "address": fields.String(required=True, description="talent's address"),
            "genre_id": fields.Integer(required=True, description="talent's address"),
            "birthday": fields.Date(required=True, description="talent's birthday"),
            "education_level_id": fields.Integer(
                required=False, description="talent's level education"
            ),
        },
    )


class UserDto:
    api = Namespace("genre", description="genre related operations")
    user = api.model(
        "user",
        {
            "email": fields.String(required=True, description="user email address"),
            "password": fields.String(required=True, description="user password"),
        },
    )


class LlankayTalentDto:
    api = Namespace(
        "LlankayTalent", description="Talents for Llankay app related operations"
    )
    empleo = api.model(
        "description",
        {
            "description": fields.String(required=False, description="Talent's current job"),
        },
    )
    education = api.model(
        "name",
        {
            "name": fields.String(required=False, description="Institution name"),
            "year_graduation": fields.String(required=False, description="Year of graduation"),
            "titulo": fields.String(required=False, description="Tittle "),

        },
    )
    llankay_talent = api.clone(
        "llankayTalent",
        TalentDto.talent,
        {
            "empleo": fields.List(fields.Nested(empleo)),
            "education": fields.List(fields.Nested(education)),
        },
    )
    genre = api.model(
        "genre",
        {
            "id": fields.Integer(required=False, description="id genre"),
            "name": fields.String(required=True, description="name genre"),

        },
    )
    residence = api.model(
        "residence",
        {
            "id": fields.Integer(required=False, description="id residence"),
            "name": fields.String(required=True, description="name residence"),

        },
    )
    education_level = api.model(
        "education_level",
        {
            "id": fields.Integer(required=False, description="id education_level"),
            "name": fields.String(required=True, description="name education_level"),

        },
    )


class InscriptionTalentDto:
    api = Namespace(
        "InscriptionTalent", description="Talents inscriptions related operations"
    )
    conocimiento_level = api.model(
        "knowledge_level",
        {
            "knowledge": fields.Integer(
                required=True, description="Talent's Knowledge"
            ),
            "level": fields.Integer(
                required=True, description="Talent's level Knowledge"
            ),
        },
    )
    empleo = api.model(
        "empleo",
        {
            "empleo": fields.Integer(required=True, description="Talent's current job"),
        },
    )
    motivation = api.model(
        "motivation",
        {
            "motivation": fields.Integer(
                required=True, description="Talent's motivation of courses"
            )
        },
    )
    curso = api.model(
        "course",
        {
            "course": fields.Integer(
                required=True, description="Talent's course interested"
            )
        },
    )
    inscription_talent = api.clone(
        "inscriptionTalent",
        TalentDto.talent,
        {
            "knowledge": fields.List(fields.Nested(conocimiento_level)),
            "empleo": fields.Nested(empleo),
            "motivation": fields.Nested(motivation),
            "course": fields.List(fields.Nested(curso))
        },
    )
