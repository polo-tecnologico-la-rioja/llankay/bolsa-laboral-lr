from flask_restx import Namespace, fields


class LlankayEmpresaDto:
    api = Namespace("Empresa", description="Empresa related operations")
    empresa = api.model(
        "empresa",
        {
            "name": fields.String(required=True, description="empresa's name"),
            "description": fields.String(required=True, description="empresa description"),
            "email": fields.String(required=True, description="empresa's email address"),
            "phone": fields.String(required=True, description="empresa's phone"),
            "webpage": fields.String(required=False, description="empresa's webpage"),
            "address": fields.String(required=True, description="empresa's address"),
        },
    )
