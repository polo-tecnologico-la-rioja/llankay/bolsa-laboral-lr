from flask_restx import Namespace, fields


class PublicDto:
    api = Namespace("Public", description="Public endpoints")
    provinces = api.model(
        "province",
        {
            "name": fields.String(required=True, description="province's name"),
        },
    )
