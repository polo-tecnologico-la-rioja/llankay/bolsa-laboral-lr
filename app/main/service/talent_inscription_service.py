from typing import Dict, Generator

from app.main import db
from app.main.model.inscription_courses import InscriptionCourses
from app.main.model.talent import Talent
from app.main.model.talent_empleo import TalentEmpleo
from app.main.model.talent_knowledges import TalentKnowledge
from app.main.model.talent_motivation import TalentMotivation
from app.main.model.user import Users
from app.main.utils.db_utils import save_changes, get_all


def _save_talent_empleo(talent_id: int, empleo_id: int):
    talent_empleo = TalentEmpleo(talent_id, empleo_id)
    return save_changes(talent_empleo)


def _save_talent_motivation(talent_id: int, motivation_id: int):
    talent_motivation = TalentMotivation(talent_id, motivation_id)
    return save_changes(talent_motivation)


def _save_talent_knowledge(talent_id: int, knowledge_id: int, level_knowledge_id: int):
    talent_knowledge = TalentKnowledge(talent_id, knowledge_id, level_knowledge_id)
    return save_changes(talent_knowledge)


def _save_talent_inscription_courses(talent_id: int, course_id: int):
    inscription = InscriptionCourses(talent_id, course_id)
    return save_changes(inscription)


def save_info_related_to_talent(
    talent_id: int, data: Dict
) -> Generator[bool, None, None]:
    print("saving talent_empleo data")
    yield _save_talent_empleo(talent_id, data["empleo"])

    print("saving talent motivation")
    yield _save_talent_motivation(talent_id, data["motivation"])

    print("saving talent knowledge")
    for knowledge in data["knowledge"]:
        yield _save_talent_knowledge(
            talent_id, knowledge["knowledge"], knowledge["level"])

    print("saving talents courses")
    for course in data["course"]:
        yield _save_talent_inscription_courses(talent_id, course["course"])


def save_new_talent(data: Dict, user_id):
    talent = Talent.query.filter_by(email=user_id).first()
    user = Users.query.filter_by(email=user_id).first()
    if not talent:
        new_talent = Talent(
            user_id=user.id,
            name=data["name"],
            surname=data["surname"],
            email=user_id,
            dni=data["dni"],
            cellphone=data["cellphone"],
            linkedin=data["linkedin"],
            other_webpage=data["other_webpage"],
            bio=data["bio"],
            github=data["github"],
            gitlab=data["gitlab"],
            birthday=data["birthday"],
            address=data["address"],
            larioja_depto_id=data["residence"],
            genre_id=data["genre"],
            education_level_id=data["education_level"],
        )

        result_new_talent = save_changes(new_talent)
        talent_object = Talent.query.filter_by(email=user_id).first()
        result_intermediate = all(
            save_info_related_to_talent(talent_object.id, data)
        )

        if all([result_intermediate, result_new_talent]):
            response_object = {
                "status": "success",
                "message": "Talent Successfully registered.",
            }
            return response_object, 201  # Talent created ok
        return 500  # Error in the save on DB (see logs)
    response_object = {"status": "fail",
                       "message": "Talent already exists.",
                       }
    return response_object, 409  # Talent Already exists.


def edit_talent(data, user_id):
    talent = Talent.query.filter_by(email=user_id).first()
    talent.name = data["name"]
    talent.surname = data["surname"]
    talent.email = user_id
    talent.dni = data["dni"]
    talent.cellphone = data["cellphone"]
    talent.linkedin = data["linkedin"]
    talent.other_webpage = data["other_webpage"]
    talent.bio = data["bio"]
    talent.github = data["github"]
    talent.gitlab = data["gitlab"]
    talent.birthday = data["birthday"]
    talent.address = data["address"]
    talent.larioja_depto_id = data["residence"]
    talent.genre_id = data["genre"]
    talent.education_level_id = data["education_level"]
    save_info_related_to_talent(talent.id, data)

    db.session.commit()
    response_object = {
        "status": "success",
        "message": "Talent Updated.",
    }
    return response_object, 201


def match(user_id):
    # This taking into account that the identity is the user_id,
    # in case you prefer a username you will only have to make small
    # modifications in the tests, in this field below and in the designation
    # of the foreignkey in the talent model
    user = Users.query.filter_by(id=user_id).first()
    talent = Talent.query.filter_by(email=user.email).first()

    if user_id and talent:
        talent.user_id = user_id
        save_changes(talent)
        response_object = {"status": "success",
                           "message": "Talent and user match related.",
                           }
        return response_object, 201
    response_object = {"status": "fail",
                       "message": "For this user, the talent data don't exist.",
                       }
    return response_object, 409


def get_a_talent(email: str):
    return (
        db.session.query(Talent)
        .filter(Talent.email == email)
        .first()
    )


def get_all_talent():
    return get_all(Talent)
