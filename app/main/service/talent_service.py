from typing import Dict, Generator

from flask_restx import abort
from http import HTTPStatus
from flask import jsonify
from datetime import datetime
import dateutil.parser

from app.main import db
from app.main.model.talent import Talent
from app.main.model.empleo import Empleo
from app.main.model.education import Education
from app.main.model.user import Users
from app.main.model.genre import Genre
from app.main.model.larioja_depto import LaRiojaDepto
from app.main.model.education_level import EducationLevel
from app.main.utils.db_utils import save_changes, get_all


def _save_talent_empleo(talent_id: int, empleo_desc: str):
    talent_empleo = Empleo(talent_id, empleo_desc)
    return save_changes(talent_empleo)


def _save_talent_education(talent_id: int, name_desc: str, year_graduation: str, titulo: str):
    talent_education = Education(talent_id, name_desc, year_graduation, titulo)
    return save_changes(talent_education)


def save_info_related_to_talent(
    talent_id: int, data: Dict
) -> Generator[bool, None, None]:
    print("saving talent_empleo data")
    for empleo in data["empleo"]:
        yield _save_talent_empleo(
            talent_id, empleo["description"])

    print("saving talent_education data")
    for education in data["education"]:
        yield _save_talent_education(
            talent_id, education["name"], education["year_graduation"], education["titulo"])


def save_new_talent(data: Dict, user_id):
    talent = Talent.query.filter_by(email=user_id).first()
    user = Users.query.filter_by(email=user_id).first()
    if not talent:
        new_talent = Talent(
            user_id=user.id,
            name=data["name"],
            surname=data["surname"],
            email=user_id,
            dni=data["dni"],
            cellphone=data["cellphone"],
            linkedin=data["linkedin"],
            other_webpage=data["other_webpage"],
            bio=data["bio"],
            github=data["github"],
            gitlab=data["gitlab"],
            birthday=dateutil.parser.isoparse(data["birthday"]),
            address=data["address"],
            larioja_depto_id=data["residence"],
            genre_id=data["genre"],
            education_level_id=data["education_level"],
        )

        result_new_talent = save_changes(new_talent)
        talent_object = Talent.query.filter_by(email=user_id).first()
        result_intermediate = all(
            save_info_related_to_talent(talent_object.id, data)
        )

        if all([result_intermediate, result_new_talent]):
            response = jsonify(
                status="success",
                message="Talent Successfully registered.",
            )
            response.status_code = HTTPStatus.CREATED
            return response
        return HTTPStatus.INTERNAL_SERVER_ERROR
    response = jsonify(
        status="fail",
        message=f"Talent already exists. With {data['email']}.",
    )
    response.status_code = HTTPStatus.CONFLICT
    return response


def edit_talent(data, user_id):
    empleos_obj = []
    educations_obj = []
    db.session.query(Education).delete()
    db.session.query(Empleo).delete()
    db.session.commit()

    talent = Talent.query.filter_by(email=user_id).first()
    talent.name = data["name"]
    talent.surname = data["surname"]
    talent.email = user_id
    talent.dni = data["dni"]
    talent.cellphone = data["cellphone"]
    talent.linkedin = data["linkedin"]
    talent.other_webpage = data["other_webpage"]
    talent.bio = data["bio"]
    talent.github = data["github"]
    talent.gitlab = data["gitlab"]
    talent.birthday = dateutil.parser.isoparse(data["birthday"])
    talent.address = data["address"]
    talent.larioja_depto_id = data["residence"]
    talent.genre_id = data["genre"]
    talent.education_level_id = data["education_level"]
    for empleos_dict in data["empleo"]:
        empleos_dict.update({"talent_id": talent.id})
        empleos_obj.append(empleos_dict)

    for educations_dict in data["education"]:
        educations_dict.update({"talent_id": talent.id})
        educations_obj.append(educations_dict)

    db.session.bulk_insert_mappings(Empleo, empleos_obj)
    db.session.bulk_insert_mappings(Education, educations_obj)

    user = Users.query.filter_by(email=user_id).first()
    user.is_profile_complete = True

    db.session.commit()
    response = jsonify(
        status="success",
        message="Talent Successfully updated.",
    )
    response.status_code = HTTPStatus.ACCEPTED
    return response


def get_a_talent(email: str):
    return (
        db.session.query(Talent)
        .filter(Talent.email == email)
        .first()
    )


def get_all_talent():
    return get_all(Talent)


def get_all_genre():
    return get_all(Genre)


def get_all_residence():
    return get_all(LaRiojaDepto)


def get_all_education_level():
    return get_all(EducationLevel)
