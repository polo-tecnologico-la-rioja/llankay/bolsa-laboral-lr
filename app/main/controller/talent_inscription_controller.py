from flask import request
from flask_restx import Resource, fields
from flask_jwt_extended import jwt_required, get_jwt_identity

from app.main.service.talent_inscription_service import save_new_talent, match, get_a_talent, edit_talent
from app.main.utils.dtos.talent_dto import InscriptionTalentDto
from app.main.utils.decorators.role import admin_required


api = InscriptionTalentDto.api
_inscription_talent = InscriptionTalentDto.inscription_talent

response_model = api.model(
    "response_inscription",
    {
        "status": fields.String,
        "message": fields.String,
    },
)


@api.route("/create_new")
class TalentNew(Resource):

    @api.doc(security='Bearer')
    @api.doc("Talent registration")
    @api.expect(_inscription_talent, validate=False)
    @api.response(code=201, model=response_model, description="Save Successful")
    @jwt_required()
    def post(self):
        """Create a new Talent"""
        talent = request.json
        user = get_jwt_identity()
        return save_new_talent(talent, user)


@api.route("/get_talent")
class TalentGet(Resource):
    @api.marshal_list_with(_inscription_talent)
    @api.doc(security='Bearer')
    @jwt_required()
    def get(self):
        """Get self Talent"""
        user = get_jwt_identity()
        return get_a_talent(user)


@api.route("/edit_talent")
class TalentUpdate(Resource):
    @api.doc(security='Bearer')
    @jwt_required()
    def put(self):
        """Edit Talent"""
        talent = request.json
        user = get_jwt_identity()
        return edit_talent(talent, user)


@api.route("/edit_talent_no_user")
class TalentNewWithoutUser(Resource):
    """Register/Update Talent"""

    @api.doc(security='Bearer')
    @api.doc("Talent registration without a user")
    @api.expect(_inscription_talent, validate=True)
    @api.response(code=201, model=response_model, description="Save Successful")
    @jwt_required()
    def post(self):
        """Create a new talent without registration user"""
        talent = request.json
        user = None
        return save_new_talent(talent, user)


@api.route("/match")
class TalentMatch(Resource):

    @api.doc(security='Bearer')
    @api.doc("After User registration if talent exist, match them")
    @api.expect(_inscription_talent, validate=True)
    @api.response(code=201, model=response_model, description="User an talent matched")
    @api.response(code=409, description="For this user, the Talent data don't exist.")
    @jwt_required()
    def post(self):
        """Matching when talent don't have a registration user before create it"""
        user = get_jwt_identity()
        return match(user)
