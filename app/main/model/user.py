from app.main import db
from app.main import flask_bcrypt
from app.main.model.user_rol import Role


class Users(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    password_hash = db.Column(db.String(100))
    email = db.Column(db.String(50), unique=True, nullable=False)
    register_on = db.Column(db.DateTime, nullable=False)
    last_pass_change = db.Column(db.DateTime, nullable=True)
    last_login = db.Column(db.DateTime, nullable=True)
    validated_mail = db.Column(db.Boolean, nullable=True, default=False)
    photo = db.Column(db.String(50), nullable=True)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'), nullable=True)
    is_profile_complete = db.Column(db.Boolean, nullable=True, default=False)
    is_password_regenerated = db.Column(db.Boolean, nullable=True, default=False)

    # TODO: Fix the logic about how to do this. Many to many new table is need, isn't?
    # technologies = db.relationship('Technologies', backref=db.backref('techs',
    #                                                                   lazy=True))
    @property
    def password(self):
        raise AttributeError("password: write-only field")

    @password.setter
    def password(self, password):
        self.password_hash = flask_bcrypt.generate_password_hash(password).decode(
            "utf-8"
        )

    def check_password(self, password):
        return flask_bcrypt.check_password_hash(self.password_hash, password)

    def __repr__(self):
        return f"<User {self.email}"
