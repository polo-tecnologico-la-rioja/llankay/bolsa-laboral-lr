from app.main import db


class Education(db.Model):
    __tablename__ = "education"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    year_graduation = db.Column(db.String(255), nullable=False)
    titulo = db.Column(db.String(255), nullable=False)
    talent_id = db.Column(db.Integer, db.ForeignKey("talent.id"), nullable=True)

    def __init__(self, talent_id: int, name_desc: str, year_graduation: str, titulo: str):
        self.talent_id = talent_id
        self.name = name_desc
        self.year_graduation = year_graduation
        self.titulo = titulo

