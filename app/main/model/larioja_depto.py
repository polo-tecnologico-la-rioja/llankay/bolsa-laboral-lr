from app.main import db


class LaRiojaDepto(db.Model):
    __tablename__ = "larioja_depto"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
