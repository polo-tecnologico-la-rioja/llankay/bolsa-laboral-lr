from app.main import db


class TalentMotivation(db.Model):
    __tablename__ = "talent_motivation"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    talent_id = db.Column(db.Integer, db.ForeignKey("talent.id"), nullable=True)
    motivation_id = db.Column(db.Integer, db.ForeignKey("motivation.id"), nullable=True)

    def __init__(self, talent_id: int, motivation_id: int):
        self.talent_id = talent_id
        self.motivation_id = motivation_id
