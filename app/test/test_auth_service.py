import unittest

from flask_testing import TestCase

from run import app, db
from app.main.service.auth_helper import (
    check_if_token_in_blocklist,
    Logout,
    Auth

)
from app.main.service.user_service import save_new_user
from flask_jwt_extended import create_access_token, decode_token
from . import BaseTestClass


class Test_Auth_helperServices(BaseTestClass):

    def test_check_if_token_in_blocklist(self):
        header = ""
        decoded = decode_token(create_access_token(identity=1))
        jti=decoded["jti"]
        Logout.save_revoked_token(jti=jti)
        assert check_if_token_in_blocklist(header, decoded)

    def test_login(self):
        data = dict()
        data["email"] = "Emmanuel"
        data["password"] = "correct_password"
        save_new_user(data)
        role, response_code = Auth.login_user(data)
        self.assertEqual(200, response_code)
        self.assertEqual(None, role)
        data["password"] = "wrong_password"
        role, response_code = Auth.login_user(data)
        self.assertEqual(401, response_code)


if __name__ == "__main__":
    unittest.main()
