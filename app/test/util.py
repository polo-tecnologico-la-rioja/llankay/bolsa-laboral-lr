"""Shared functions and constants for unit tests."""

from flask import url_for
from run import app, db
from app.main.model.user import Users
from app.main.model.user_rol import Role

EMAIL = "new_user@email.com"
PASSWORD = "test1234"


def register_user(email, password):
    with app.test_client() as c:
        response = c.post(
            url_for("api.user_user_register"),
            json={
                'email': email, 'password': password
            },
            content_type="application/json",
        )
        return response


def register_role(name, token):
    with app.test_client() as c:
        response = c.post(
            url_for("api.user_role_new"),
            json={
                'name': name,
            },
            content_type="application/json",
            headers={"Authorization": f"Bearer {token}"},
        )
        return response


def login_user(email, password):
    with app.test_client() as c:
        response = c.post(
            url_for("api.auth_user_login"),
            json={
                'email': email, 'password': password
            },
            content_type="application/json",
        )
        return response


def administrator_user():
    role1 = Role(
        name="admin",
    )
    db.session.add(role1)
    db.session.commit()
    register_user(EMAIL, PASSWORD)
    user = Users.query.filter_by(email=EMAIL).first()
    user.role_id = 1
    db.session.commit()
    return login_user(EMAIL, PASSWORD)
