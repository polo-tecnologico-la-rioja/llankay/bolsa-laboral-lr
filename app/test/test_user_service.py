import unittest
import datetime

from http import HTTPStatus

from run import db
from app.main.service.user_service import (
    save_new_user,
    get_all_users,
    get_a_user,
    edit_role_user_administrator,
)

from app.main.model.user import Users
from app.main.model.user_rol import Role
from . import BaseTestClass


class TestUserServices(BaseTestClass):

    def test_save_new_user(self):
        data = dict()
        data["password"] = "hola"
        data["email"] = "eamanu@yaerobi.com"

        save_new_user(data)

        user_result = Users.query.filter_by(email="eamanu@yaerobi.com").first()

        self.assertEqual("eamanu@yaerobi.com", user_result.email)

    def test_get_all_users(self):
        self.assertEqual([], get_all_users())

        user1 = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu1@yaerobi",
        )
        user2 = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu2@yaerobi",
        )

        user3 = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu3@yaerobi",
        )
        db.session.add(user1)
        db.session.add(user2)
        db.session.add(user3)

        db.session.commit()

        all_users = get_all_users()

        self.assertEqual(3, len(all_users))
        for i, user in enumerate(all_users):
            self.assertEqual(f"eamanu{i+1}@yaerobi", user.email)

    def test_get_a_user(self):
        self.assertEqual([], get_all_users())

        user1 = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu@yaerobi",
            # country_city_id="1",
        )
        user2 = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu2@yaerobi",
            # country_city_id="1",
        )

        user3 = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu3@yaerobi",
            # country_city_id="1",
        )
        db.session.add(user1)
        db.session.add(user2)
        db.session.add(user3)

        db.session.commit()

        user = get_a_user("eamanu@yaerobi")
        self.assertEqual("eamanu@yaerobi", user.email)
        self.assertEqual("hola", user.password_hash)

    def test_response_409_if_already_exist_email(self):
        self.assertEqual([], get_all_users())

        data = dict()
        data["password"] = "hola"
        data["email"] = "eamanu@yaerobi.com"

        response = save_new_user(data)

        self.assertEqual(response.json["status"], "success")
        self.assertEqual(response.json["message"], "successfully registered")
        self.assertEqual(HTTPStatus.CREATED, response.status_code)

        data = dict()
        data["password"] = "hola"
        data["email"] = "eamanu@yaerobi.com"

        response = save_new_user(data)

        self.assertEqual(HTTPStatus.CONFLICT, response.status_code)
        self.assertEqual(response.json["message"], "Email eamanu@yaerobi.com, is already registered")

    def test_edit_userRole(self):
        self.assertEqual([], get_all_users())

        data = dict()
        data["email"] = "eamanu@yaerobi"
        data["role_id"] = 1

        user1 = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu@yaerobi",
        )
        role1 = Role(
            name="admin",
        )

        db.session.add(user1)
        db.session.add(role1)

        db.session.commit()

        response = edit_role_user_administrator(data)
        user = get_a_user("eamanu@yaerobi")
        self.assertEqual("eamanu@yaerobi", user.email)
        self.assertEqual(1, user.role_id)
        self.assertEqual(HTTPStatus.ACCEPTED, response.status_code)
        self.assertEqual(response.json["message"], "User Updated.")


if __name__ == "__main__":
    unittest.main()
