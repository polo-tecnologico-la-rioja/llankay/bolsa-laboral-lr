"""Unit tests for api.empresa_... API endpoint."""
from http import HTTPStatus
import unittest
from datetime import datetime
from run import app, db
from app.test.util import administrator_user, register_user
from . import BaseTestClass
from flask import url_for
from app.main.model.empresa import Empresa


class TestEmpresaController(BaseTestClass):

    def test_empresa_register(self):
        token = administrator_user()

        with app.test_client() as c:
            response = c.post(
                url_for("api.Empresa_empresa_new"),

                json={"name": "Emmanuel",
                      "description": "Arias",
                      "email": "eamanu@yaerobi.com",
                      "phone": "2",
                      "webpage": "other_webpage2",
                      "address": "Fatima calle 2",
                      },

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.CREATED, response.status_code)
            self.assertEqual(response.json["message"], "Empresa Successfully registered.")

    def test_empresa_already_registered(self):
        token = administrator_user()

        with app.test_client() as c:
            response = c.post(
                url_for("api.Empresa_empresa_new"),

                json={"name": "Emmanuel",
                      "description": "Arias",
                      "email": "eamanu@yaerobi.com",
                      "phone": "2",
                      "webpage": "other_webpage2",
                      "address": "Fatima calle 2",
                      },

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.CREATED, response.status_code)
            self.assertEqual(response.json["message"], "Empresa Successfully registered.")

            response = c.post(
                url_for("api.Empresa_empresa_new"),

                json={"name": "Emmanuel",
                      "description": "Arias",
                      "email": "eamanu@yaerobi.com",
                      "phone": "2",
                      "webpage": "other_webpage2",
                      "address": "Fatima calle 2",
                      },

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
            )
            self.assertEqual(HTTPStatus.CONFLICT, response.status_code)
            self.assertEqual(response.json["message"], "Empresa already exists. With user eamanu@yaerobi.com.")

    def test_empresa_updated(self):
        token = administrator_user()

        with app.test_client() as c:
            response = c.post(
                url_for("api.Empresa_empresa_new"),

                json={"name": "Emmanuel",
                      "description": "Arias",
                      "email": "eamanu@yaerobi.com",
                      "phone": "2",
                      "webpage": "other_webpage2",
                      "address": "Fatima calle 2",
                      },

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.CREATED, response.status_code)
            self.assertEqual(response.json["message"], "Empresa Successfully registered.")

            response = c.put(
                url_for("api.Empresa_empresa_update"),

                json={"name": "Favbi SRL",
                      "description": "Famas",
                      "email": "eamanu@yaerobi.com",
                      "phone": "2",
                      "webpage": "other_webpage2",
                      "address": "Fatima calle 2",
                      },

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
            )
            self.assertEqual(HTTPStatus.ACCEPTED, response.status_code)
            self.assertEqual(response.json["message"], "Empresa Successfully updated.")

    def test_empresa_get_controller(self):
        token = administrator_user()
        with app.test_client() as c:
            response = c.post(
                url_for("api.Empresa_empresa_new"),

                json={"name": "Emmanuel",
                      "description": "Arias",
                      "email": "eamanu@yaerobi.com",
                      "phone": "2",
                      "webpage": "other_webpage2",
                      "address": "Fatima calle 2",
                      },

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
            )
            self.assertEqual(HTTPStatus.CREATED, response.status_code)
            self.assertEqual(response.json["message"], "Empresa Successfully registered.")

        with app.test_client() as c:
            response = c.get(
                url_for("api.Empresa_empresa_get"),
                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.OK, response.status_code)

    def test_empresa_get_all_controller(self):
        [register_user(f"eamanu{i}@yaerobi", "hola") for i in range(3)]
        empresas = [Empresa(user_id=1,
                            name="Ricardo",
                            description="Andino",
                            email=f"eamanu{i}@yaerobi",
                            phone="1",
                            webpage="other_webpage",
                            address="Fatima calle 1"
                                   ) for i in range(3)]
        [db.session.add(n) for n in empresas]
        db.session.commit()
        token = administrator_user()
        with app.test_client() as c:
            all_empresa = c.get(
                url_for("api.Empresa_empresa_list"),
                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.OK, all_empresa.status_code)
            self.assertEqual(3, len(all_empresa.json))
            [self.assertEqual(f"eamanu{i}@yaerobi", empresa["email"])
             for i, empresa in enumerate(all_empresa.json) if i < 3]


if __name__ == "__main__":
    unittest.main()
