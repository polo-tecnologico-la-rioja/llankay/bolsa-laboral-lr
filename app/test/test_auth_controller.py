"""Unit tests for api.auth_... API endpoint."""
from http import HTTPStatus
import unittest
from run import app
from app.test.util import EMAIL, PASSWORD, administrator_user, register_user
from . import BaseTestClass
from flask import url_for
from flask_jwt_extended import decode_token
from app.main.service.auth_helper import (
    check_if_token_in_blocklist,

)


class TestAuthController(BaseTestClass):

    def test_auth_login(self):
        register_user(EMAIL, PASSWORD)
        with app.test_client() as c:
            response = c.post(
                url_for("api.auth_user_login"),

                json={
                    'email': EMAIL,
                    'password': PASSWORD,
                },
                content_type="application/json",
            )
            token = decode_token(response.json["access_token"])
            refresh = decode_token(response.json["refresh_token"])
            self.assertEqual(token["sub"], EMAIL)
            self.assertEqual(refresh["sub"], EMAIL)
            self.assertEqual(HTTPStatus.ACCEPTED, response.status_code)
        with app.test_client() as c:
            response = c.post(
                url_for("api.auth_user_login"),

                json={
                    'email': "fake",
                    'password': PASSWORD,
                },
                content_type="application/json",
            )
            self.assertEqual(HTTPStatus.CONFLICT, response.status_code)
            self.assertEqual("Bad username or password.", response.json["message"])

    def test_auth_logout_access(self):
        header = ""
        token = administrator_user()
        decoded = decode_token(token.json['access_token'])
        self.assertEqual(False, check_if_token_in_blocklist(header, decoded))
        with app.test_client() as c:
            response = c.post(
                url_for("api.auth_logout_access"),

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
            )
            decoded = decode_token(token.json['access_token'])
            self.assertEqual(True, check_if_token_in_blocklist(header, decoded))
            self.assertEqual(HTTPStatus.OK, response.status_code)
            self.assertEqual("access token has been revoked.", response.json["message"])
        with app.test_client() as c:
            all_users = c.get(
                url_for("api.user_user_list"),
                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.UNAUTHORIZED, all_users.status_code)

    def test_auth_logout_refresh(self):
        header = ""
        token = administrator_user()
        decoded = decode_token(token.json['refresh_token'])
        self.assertEqual(False, check_if_token_in_blocklist(header, decoded))
        with app.test_client() as c:
            response = c.post(
                url_for("api.auth_logout_refresh"),

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['refresh_token']}"},
            )
            decoded = decode_token(token.json['refresh_token'])
            self.assertEqual(True, check_if_token_in_blocklist(header, decoded))
            self.assertEqual(HTTPStatus.OK, response.status_code)
            self.assertEqual("refresh token has been revoked.", response.json["message"])
        with app.test_client() as c:
            response = c.post(
                url_for("api.auth_token_refresh"),
                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['refresh_token']}"},
                )
            self.assertEqual(HTTPStatus.UNAUTHORIZED, response.status_code)


if __name__ == "__main__":
    unittest.main()
