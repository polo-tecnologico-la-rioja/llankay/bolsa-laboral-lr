import unittest
import datetime
from http import HTTPStatus

from run import db
from app.main.service.user_service import (
    get_all_users,
)
from app.main.service.talent_service import (
    save_new_talent,
    edit_talent,
    get_all_talent,
    get_a_talent,
    get_all_genre,
    get_all_education_level,
    get_all_residence
)
from app.main.model.user import Users
from app.main.model.talent import Talent
from app.main.model.genre import Genre
from app.main.model.education_level import EducationLevel
from app.main.model.larioja_depto import LaRiojaDepto
from app.main.model.empleo import Empleo
from app.main.model.education import Education
from . import BaseTestClass


class TestTalentServices(BaseTestClass):

    def test_save_new_talent(self):
        user = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu@yaerobi.com",
        )
        db.session.add(user)
        db.session.commit()

        data = dict()
        data["name"] = "Emmanuel"
        data["surname"] = "Arias"
        data["email"] = "eamanu@yaerobi.com"
        data["dni"] = "2"
        data["cellphone"] = "2"
        data["linkedin"] = "linkedin2"
        data["other_webpage"] = "other_webpage2"
        data["bio"] = "bio2"
        data["github"] = "github2"
        data["gitlab"] = "gitlab2"
        data["birthday"] = datetime.datetime(2019, 5, 18, 15, 17, 8, 132263).isoformat()
        data["address"] = "Fatima calle 2"
        data["residence"] = 1
        data["genre"] = 1
        data["education"] = [{"name": "UNLAR0", "year_graduation": 2010, "titulo": "INGENIERO"},
                             {"name": "UNLAR1", "year_graduation": 2011, "titulo": "Contador"}]
        data["education_level"] = 1
        data["empleo"] = [{"description": "vendedor0"}, {"description": "vendedor1"}]

        save_new_talent(data, "eamanu@yaerobi.com")

        talent_result = Talent.query.filter_by(email="eamanu@yaerobi.com").first()
        empleo_result = list(Empleo.query.filter_by(talent_id=talent_result.id))
        education_result = list(Education.query.filter_by(talent_id=talent_result.id))

        self.assertEqual("eamanu@yaerobi.com", talent_result.email)
        for i, empleo in enumerate(empleo_result):
            self.assertEqual(f"vendedor{i}", empleo.description)
        for i, education in enumerate(education_result):
            self.assertEqual(f"UNLAR{i}", education.name)

    def test_get_all_talent(self):
        self.assertEqual([], get_all_users())

        user1 = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="ricardo@yaerobi",
        )
        user2 = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu2@yaerobi",
        )
        db.session.add(user1)
        db.session.add(user2)
        db.session.commit()

        data = dict()
        data["name"] = "Ricardo"
        data["surname"] = "Andino"
        data["email"] = "ricardo@yaerobi"
        data["dni"] = "1"
        data["cellphone"] = "2"
        data["linkedin"] = "linkedin"
        data["other_webpage"] = "other_webpage2"
        data["bio"] = "bio"
        data["github"] = "github2"
        data["gitlab"] = "gitlab2"
        data["birthday"] = datetime.datetime(2019, 5, 18, 15, 17, 8, 132263).isoformat()
        data["address"] = "Fatima calle 2"
        data["residence"] = 1
        data["genre"] = 1
        data["education"] = [{"name": "UNLAR0", "year_graduation": 2010, "titulo": "INGENIERO"},
                             {"name": "UNLAR1", "year_graduation": 2011, "titulo": "Contador"}]
        data["education_level"] = 1
        data["empleo"] = [{"description": "vendedor0"}, {"description": "vendedor1"}]

        save_new_talent(data, "ricardo@yaerobi")

        data["name"] = "Emmanuel"
        data["surname"] = "Arias"
        data["email"] = "eamanu2@yaerobi"
        data["dni"] = "2"
        data["cellphone"] = "2"
        data["linkedin"] = "linkedin"
        data["other_webpage"] = "other_webpage2"
        data["bio"] = "bio"
        data["github"] = "github2"
        data["gitlab"] = "gitlab2"
        data["birthday"] = datetime.datetime(2019, 5, 18, 15, 17, 8, 132263).isoformat()
        data["address"] = "Fatima calle 2"
        data["residence"] = 1
        data["genre"] = 1
        data["education"] = [{"name": "UNLAR0", "year_graduation": 2010, "titulo": "INGENIERO"},
                             {"name": "UNLAR1", "year_graduation": 2011, "titulo": "Contador"}]
        data["education_level"] = 1
        data["empleo"] = [{"description": "vendedor0"}, {"description": "vendedor1"}]

        save_new_talent(data, "eamanu2@yaerobi")

        all_candidate = get_all_talent()

        self.assertEqual(2, len(all_candidate))
        for talent in all_candidate:
            user = Users.query.filter_by(email=talent.email).first()
            self.assertEqual(talent.email, user.email)
            self.assertIn(f"bio", talent.bio)
            self.assertIn(f"linkedin", talent.linkedin)

    def test_get_all_genre(self):
        self.assertEqual([], get_all_users())

        genre = [Genre(name=f"identity{i}") for i in range(3)]
        [db.session.add(n) for n in genre]
        db.session.commit()

        all_genre = get_all_genre()

        self.assertEqual(3, len(all_genre))
        [self.assertEqual(f"identity{i}", genre.name)
         for i, genre in enumerate(all_genre) if i < 3]

    def test_get_all_educationlevel(self):
        self.assertEqual([], get_all_users())

        educlevel = [EducationLevel(name=f"level{i}") for i in range(3)]
        [db.session.add(n) for n in educlevel]
        db.session.commit()

        all_educlevel = get_all_education_level()

        self.assertEqual(3, len(all_educlevel))
        [self.assertEqual(f"level{i}", educlevel.name)
         for i, educlevel in enumerate(all_educlevel) if i < 3]

    def test_get_all_residence(self):
        self.assertEqual([], get_all_users())

        residence = [LaRiojaDepto(name=f"place{i}") for i in range(3)]
        [db.session.add(n) for n in residence]
        db.session.commit()

        all_residence = get_all_residence()

        self.assertEqual(3, len(all_residence))
        [self.assertEqual(f"place{i}", residence.name)
         for i, residence in enumerate(all_residence) if i < 3]

    def test_get_a_talent(self):
        self.assertEqual([], get_all_users())
        user1 = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="ricardo@yaerobi",
        )
        user2 = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu2@yaerobi",
        )
        db.session.add(user1)
        db.session.add(user2)
        db.session.commit()

        data = dict()
        data["name"] = "Ricardo"
        data["surname"] = "Andino"
        data["email"] = "ricardo@yaerobi"
        data["dni"] = "1"
        data["cellphone"] = "2"
        data["linkedin"] = "linkedin"
        data["other_webpage"] = "other_webpage2"
        data["bio"] = "bio"
        data["github"] = "github2"
        data["gitlab"] = "gitlab2"
        data["birthday"] = datetime.datetime(2019, 5, 18, 15, 17, 8, 132263).isoformat()
        data["address"] = "Fatima calle 2"
        data["residence"] = 1
        data["genre"] = 1
        data["education"] = [{"name": "UNLAR0", "year_graduation": 2010, "titulo": "INGENIERO"},
                             {"name": "UNLAR1", "year_graduation": 2011, "titulo": "Contador"}]
        data["education_level"] = 1
        data["empleo"] = [{"description": "vendedor0"}, {"description": "vendedor1"}]

        save_new_talent(data, "ricardo@yaerobi")

        data["name"] = "Emmanuel"
        data["surname"] = "Arias"
        data["email"] = "eamanu2@yaerobi"
        data["dni"] = "2"
        data["cellphone"] = "2"
        data["linkedin"] = "linkedin2"
        data["other_webpage"] = "other_webpage2"
        data["bio"] = "bio2"
        data["github"] = "github2"
        data["gitlab"] = "gitlab2"
        data["birthday"] = datetime.datetime(2019, 5, 18, 15, 17, 8, 132263).isoformat()
        data["address"] = "Fatima calle 2"
        data["residence"] = 1
        data["genre"] = 1
        data["education"] = [{"name": "UNLAR0", "year_graduation": 2010, "titulo": "INGENIERO"},
                             {"name": "UNLAR1", "year_graduation": 2011, "titulo": "Contador"}]
        data["education_level"] = 1
        data["empleo"] = [{"description": "vendedor0"}, {"description": "vendedor1"}]

        save_new_talent(data, "eamanu2@yaerobi")

        talent = get_a_talent("eamanu2@yaerobi")

        self.assertEqual("Emmanuel", talent.name)
        self.assertEqual("Arias", talent.surname)
        self.assertEqual("eamanu2@yaerobi", talent.email)
        self.assertEqual("linkedin2", talent.linkedin)
        self.assertEqual("bio2", talent.bio)

        talent = get_a_talent("ricardo@yaerobi")

        self.assertEqual("Ricardo", talent.name)
        self.assertEqual("Andino", talent.surname)
        self.assertEqual("ricardo@yaerobi", talent.email)
        self.assertEqual("linkedin", talent.linkedin)
        self.assertEqual("bio", talent.bio)

    def test_response_talent_update(self):
        user = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu@yaerobi.com",
        )
        db.session.add(user)
        db.session.commit()

        data = dict()
        data["name"] = "Emmanuel"
        data["surname"] = "Arias"
        data["email"] = "eamanu@yaerobi.com"
        data["dni"] = "1"
        data["cellphone"] = "2"
        data["linkedin"] = "linkedin2"
        data["other_webpage"] = "other_webpage2"
        data["bio"] = "bio2"
        data["github"] = "github2"
        data["gitlab"] = "gitlab2"
        data["birthday"] = datetime.datetime(2019, 5, 18, 15, 17, 8, 132263).isoformat()
        data["address"] = "Fatima calle 2"
        data["residence"] = 1
        data["genre"] = 1
        data["education"] = [{"name": "UNLAR", "year_graduation": 2010, "titulo": "INGENIERO"}]
        data["education_level"] = 1
        data["empleo"] = [{"description": "vendedor"}]

        response = save_new_talent(data, "eamanu@yaerobi.com")

        self.assertEqual(HTTPStatus.CREATED, response.status_code)
        self.assertEqual(response.json["message"], "Talent Successfully registered.")

        data = dict()
        data["name"] = "Rodrigo"
        data["surname"] = "Rojas"
        data["email"] = "eamanu@yaerobi.com"
        data["dni"] = "2"
        data["cellphone"] = "2"
        data["linkedin"] = "linkedin2"
        data["other_webpage"] = "other_webpage2"
        data["bio"] = "bio2"
        data["github"] = "github2"
        data["gitlab"] = "gitlab2"
        data["birthday"] = datetime.datetime(2019, 5, 18, 15, 17, 8, 132263).isoformat()
        data["address"] = "Fatima calle 2"
        data["residence"] = 1
        data["genre"] = 1
        data["education"] = [{"name": "UNLAR", "year_graduation": 2010, "titulo": "INGENIERO"}]
        data["education_level"] = 1
        data["empleo"] = [{"description": "vendedor"}]

        response = edit_talent(data, "eamanu@yaerobi.com")

        self.assertEqual(HTTPStatus.ACCEPTED, response.status_code)
        self.assertEqual(response.json["message"], "Talent Successfully updated.")


if __name__ == "__main__":
    unittest.main()
