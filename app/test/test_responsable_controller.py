"""Unit tests for api.responsable_... API endpoint."""
from http import HTTPStatus
import unittest
from run import app, db
from app.main.model.responsable import Responsable
from app.test.util import administrator_user, register_user
from . import BaseTestClass
from flask import url_for


class TestResponsableController(BaseTestClass):

    def test_responsable_register(self):
        token = administrator_user()

        with app.test_client() as c:
            response = c.post(
                url_for("api.Responsable_responsable_new"),

                json={"name": "Emmanuel",
                      "surname": "Arias",
                      "email": "eamanu@yaerobi.com",
                      "cellphone": "2",
                      "empresa_id": None,
                      },

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.CREATED, response.status_code)
            self.assertEqual(response.json["message"], "Responsable Successfully registered.")

    def test_responsable_already_registered(self):
        token = administrator_user()

        with app.test_client() as c:
            response = c.post(
                url_for("api.Responsable_responsable_new"),

                json={"name": "Emmanuel",
                      "surname": "Arias",
                      "email": "eamanu@yaerobi.com",
                      "cellphone": "2",
                      "empresa_id": None,
                      },

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.CREATED, response.status_code)
            self.assertEqual(response.json["message"], "Responsable Successfully registered.")

            response = c.post(
                url_for("api.Responsable_responsable_new"),

                json={"name": "Emmanuel",
                      "surname": "Arias",
                      "email": "eamanu@yaerobi.com",
                      "cellphone": "2",
                      "empresa_id": None,
                      },

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
            )
            self.assertEqual(HTTPStatus.CONFLICT, response.status_code)
            self.assertEqual(response.json["message"], "Responsable already exists. With eamanu@yaerobi.com.")

    def test_responsable_updated(self):
        token = administrator_user()

        with app.test_client() as c:
            response = c.post(
                url_for("api.Responsable_responsable_new"),

                json={"name": "Emmanuel",
                      "surname": "Arias",
                      "email": "eamanu@yaerobi.com",
                      "cellphone": "2",
                      "empresa_id": None,
                      },

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.CREATED, response.status_code)
            self.assertEqual(response.json["message"], "Responsable Successfully registered.")

            response = c.put(
                url_for("api.Responsable_responsable_update"),

                json={"name": "Favbi SRL",
                      "surname": "Famas",
                      "email": "eamanu@yaerobi.com",
                      "cellphone": "2",
                      "empresa_id": None,
                      },

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
            )
            self.assertEqual(HTTPStatus.ACCEPTED, response.status_code)
            self.assertEqual(response.json["message"], "Responsable Successfully updated.")

    def test_responsable_get_controller(self):
        token = administrator_user()
        with app.test_client() as c:
            response = c.post(
                url_for("api.Responsable_responsable_new"),

                json={"name": "Emmanuel",
                      "surname": "Arias",
                      "email": "eamanu@yaerobi.com",
                      "cellphone": "2",
                      "empresa_id": None,
                      },

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
            )
            self.assertEqual(HTTPStatus.CREATED, response.status_code)
            self.assertEqual(response.json["message"], "Responsable Successfully registered.")

        with app.test_client() as c:
            response = c.get(
                url_for("api.Responsable_responsable_get"),
                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.OK, response.status_code)

    def test_responsable_get_all_controller(self):
        [register_user(f"eamanu{i}@yaerobi", "hola") for i in range(3)]
        responsable = [Responsable(user_id=(i+1),
                                   name="Ricardo",
                                   surname="Andino",
                                   email=f"eamanu{i}@yaerobi",
                                   cellphone="1",
                                   ) for i in range(3)]
        [db.session.add(n) for n in responsable]
        db.session.commit()
        token = administrator_user()
        with app.test_client() as c:
            all_responsable = c.get(
                url_for("api.Responsable_responsable_list"),
                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.OK, all_responsable.status_code)
            self.assertEqual(3, len(all_responsable.json))
            [self.assertEqual(f"eamanu{i}@yaerobi", responsable["email"])
             for i, responsable in enumerate(all_responsable.json) if i < 3]


if __name__ == "__main__":
    unittest.main()
