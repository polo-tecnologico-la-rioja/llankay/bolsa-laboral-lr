FROM python:3.10-slim-buster

WORKDIR /usr/src/app

COPY . /usr/src/app/

RUN pip3 install -r requirements.txt

ENV FLASK_APP=run.py:app

EXPOSE 5000

CMD ["python3", "-m", "flask", "run", "-h", "0.0.0.0"]
